package com.example.asistenciaj;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.view.View;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;

import java.util.Calendar;
import java.util.Date;

import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.Network;
import com.android.volley.Cache;




import com.android.volley.RequestQueue;


public class MainActivity extends AppCompatActivity {



    BluetoothAdapter mBluetoothAdapter;
    Date strDate;
    TextView textView;
    boolean yaEntro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.button);

        textView = findViewById(R.id.textView);
        yaEntro =false;
        textView.setText("Presione el boton para verificar el registro de su asistencia.");

    }

    public void onButtonClick(View view) throws Exception
    {
        textView = findViewById(R.id.textView);
        String str1 = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/";
        String str2="/students/201533772";
        String url=null;
        Calendar calendar = Calendar.getInstance();
        int dia=calendar.get(Calendar.DAY_OF_MONTH);
        int mes=calendar.get(Calendar.MONTH);
        int ano=calendar.get(Calendar.YEAR);
        String fecha=dia+"-"+mes+"-"+"ano";


        url=str1+fecha+str2;

        if(yaEntro ==false)
        {
            yaEntro =true;
            prenderElBluetooth();
            Thread.sleep(5000);

            verificarAsistencia(url);
            textView.setText("Se va a validar su asistencia. Espere por favor.");

            Thread.sleep(12000);

            if(textView.getText().equals("Se ha validado exitosamente"))
            {
                textView.setText("Se valido todo con exito y ha apagado el bluetooth.");
                apagarBluetooth();

            }
            else
            {
                textView.setText("No se ha podido verificar. Por favor vuelva a intentarlo.");

            }



        }
        else
        {
            textView.setText("Se va a iniciar la busqueda de la confirmacion de su asistencia");
            verificarAsistencia(url);
            Thread.sleep(7000);
            if(textView.getText().equals("Se ha validado exitosamente"))
            {

                apagarBluetooth();
            }
            else
            {
                textView.setText("No se ha podido verificar. Por favor vuelva a intentarlo.");
            }

        }

    }



    public void prenderElBluetooth(){

        Intent dIntent =  new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        dIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 3600);
        startActivity(dIntent);
    }
    private void apagarBluetooth() {
        mBluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        mBluetoothAdapter.disable();
        textView.setText("Se valido todo con exito y ha apagado el bluetooth.");
    }

    public void verificarAsistencia(String url){
        //String url="https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/5-7-2019/students/201533772";
        RequestQueue requestQueue;

        // Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap

        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());

        // Instantiate the RequestQueue with the cache and network.
        requestQueue = new RequestQueue(cache, network);

        // Start the queue
        requestQueue.start();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                textView = findViewById(R.id.textView);
                textView.setText("Se ha validado exitosamente");

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d( "Error: " + error.getMessage());
                textView = findViewById(R.id.textView);
                textView.setText("No se ha podido validar exitosamente");
            }
        });

        // Adding request to request queue
        requestQueue.add(strReq);




    }


}
